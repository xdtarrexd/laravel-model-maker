<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Name
    |--------------------------------------------------------------------------
    |
    | Define how we decide the Model name.
    |
    | available macros: %table_name%
    |
    */
    'name' => 'ucfirst(Str::camel(Str::singular("%table_name%")))',


    /*
    |--------------------------------------------------------------------------
    | Default id
    |--------------------------------------------------------------------------
    |
    | Default primary key for table
    |
    */

    'primary_key' => 'id',

    /*
    |--------------------------------------------------------------------------
    | Dir
    |--------------------------------------------------------------------------
    |
    | Define the basepath for the model directory
    |
    */

    'dir' => 'app\Models',

    /*
    |--------------------------------------------------------------------------
    | Namespace
    |--------------------------------------------------------------------------
    |
    | Define the default namespace for the model directory
    |
    */

    'namespace' => 'App\Models',

    /*
    |--------------------------------------------------------------------------
    | Uses
    |--------------------------------------------------------------------------
    |
    | Define the default uses for our models
    |
    */

    'uses' => ['Illuminate\Database\Eloquent\Model'],

    /*
    |--------------------------------------------------------------------------
    | Traits
    |--------------------------------------------------------------------------
    |
    | Define the default Traits for our models
    |
    */

    'traits' => [''],

    /*
    |--------------------------------------------------------------------------
    | Extends
    |--------------------------------------------------------------------------
    |
    | Define the default Extension for our models
    |
    */

    'extends' => 'Model',

    /*
    |--------------------------------------------------------------------------
    | Pop first directory
    |--------------------------------------------------------------------------
    |
    | If TRUE, the first directory after the basedir will be popped.
    | Mysql\.... -> \....
    |
    */

    'pop_first_directory' => true,

    /*
    |--------------------------------------------------------------------------
    | Connections
    |--------------------------------------------------------------------------
    |
    | The default configuration for the database connections
    |
    */

    'connections' => config('database.connections'),

    /*
    |--------------------------------------------------------------------------
    | Excluded fillables
    |--------------------------------------------------------------------------
    |
    | What we can exclude from the "protected $fillables" variable
    |
    */

    'excluded_fillables' => ['created_at', 'updated_at', 'deleted_at', 'id'],


    /*
    |--------------------------------------------------------------------------
    | Excluded tables
    |--------------------------------------------------------------------------
    |
    | Explicitly excluded tables
    | should be entered as "connection\table_name"
    |
    */

    'excluded_tables' => [
        config('database.default') . '\\' . config('database.migrations')
    ],

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    |
    | If enabled, orm-maker will attempt to solve the relationships between all
    | connections.
    |
    |
    */

    'relationships' => [
        'enabled' => true,

        //if set to true, if the model parent name is found in a relation from the beginning, that one will be remvoed
        // if the parent class name is "WebshopOrder" and the relationship method predicted name is "WebshopCategoryItems" it will changed to "CategoryItems".
        'exclude_parent_name' => true,

        //What models we want to map
        'include' => [
            'belongsto' => true,
            'hasmany' => true,
            'belongstomany' => true,
            'hasone' => true,
            'belongstomanythrough' => true,
        ],

        //With this  map, you can tell what belongs to what, and vice versa.
        //The Avilable match types are:

        // "STRICT" (Default)
        // "LOWERCASE" will match columns with forced lowercase, example: ProfileID and profileid will match
        // "SIMILAR_TEXT" will match with Php's "similar text" function, example: profile_id and ProfileID will match
        'map_match_type' => 'SIMILAR_TEXT',
        'map' => [
            // 'PropID:hasone' => 'property_id:hasmany' // PropID hasOne property_id and property_id hasMany PropID
            // 'profile_id:hasone' => 'UserID:hasone'
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Timestamps
    |--------------------------------------------------------------------------
    |
    | If enabled, orm-maker will solve everything date-related
    |
    |
    */

    'timestamps' => [
        'enabled' => true,

        'default_columns' => [
            'created_at' => null,
            'updated_at' => null,
            'deleted_at' => null
        ],

        'provider' => [
            'uses' => 'Carbon\Carbon',
            'usage' => '$v ? Carbon::parse($v) : $v',
        ],

        'mutate_other_timestamps' => true,
        'detect_softDeletes' => true,
    ]
];
