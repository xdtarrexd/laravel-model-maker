<?php
/**
 * Created by PhpStorm.
 * User: tarre
 * Date: 2017-04-06
 * Time: 14:08
 */

namespace Tarre\ModelMaker\Console;


use Illuminate\Console\Command;
use Tarre\ModelMaker\Orm\Generator;

/**
 * @property \Illuminate\Console\OutputStyle public_output
 */
class MakeModelsConsole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:models {debug?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Build models from all available Database connections';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->public_output = $this->output;
        return new Generator($this);

    }
}
