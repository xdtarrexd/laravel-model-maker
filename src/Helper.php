<?php
/**
 * Created by PhpStorm.
 * User: tarre
 * Date: 2017-04-07
 * Time: 16:39
 */

namespace Tarre\ModelMaker;

use Illuminate\Support\Str;

class Helper
{

    /**
     * @param string $table
     * @param string $text
     * @return string
     */
    public static function parse_macro(string $table, string $text): string
    {

        return eval(str_replace('%table_name%', $table, 'return ' . $text . ';')); // Because eval
    }

    /**
     * @param array $array
     * @param array $has
     * @param bool $exactly
     * @return bool
     */
    public static function has(Array $array, Array $has, $exactly = true)
    {
        $x = 0;
        foreach ($has as $ha) {
            if (in_array($ha, $array)) {
                $x++;
            }
        }
        return $exactly ? $x == count($has) : $x >= count($has);
    }

    /**
     * @param $table1
     * @param $table2
     * @return string
     */
    public static function createPivotTable($table1, $table2)
    {
        // We do this so we can sort table by name
        $tables = [$table1, $table2];
        sort($tables);
        return Str::singular($tables[0]) . '_' . Str::singular($tables[1]);
    }

    public static function array_search_keyInsensitive($needle, &$haystack, $fallback = null)
    {
        foreach ($haystack as $K => $V) {
            if (strcasecmp($needle, $K) == 0) {
                return $V;
            }
        }

        return $fallback;
    }

}
