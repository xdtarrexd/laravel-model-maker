<?php
/**
 * Created by PhpStorm.
 * User: tarre
 * Date: 2017-04-10
 * Time: 10:17
 */

namespace Tarre\ModelMaker;


use Carbon\Carbon;
use Illuminate\Support\Str;

class ClassMaker
{
    private $path;
    protected $buffer = "";
    protected $name = null;
    protected $type = 'class';
    protected $namespace = null;
    protected $implementation = null;
    protected $extension = null;
    protected $propertyInfo = [];
    protected $usages = [];
    protected $traits = [];
    protected $params = [];
    protected $consts = [];
    protected $functions = [];

    public function __construct($path, $name = null)
    {
        $this->path = $path;
        $this->name = $name ? $name : preg_replace('/.*\\\\([^.]+).*/', "$1", $path);

    }

    public function setType($type = 'class')
    {
        $this->type = $type;
        return $this;

    }

    public function setNamespace($name)
    {
        $this->namespace = $name;
        return $this;

    }

    public function setExtension($name)
    {
        $this->extension = $name;
        return $this;

    }

    public function setImplementation($name)
    {
        $this->implementation = $name;
        return $this;

    }

    public function addParamPropertyInfo($param, $references)
    {
        $this->propertyInfo[$param] = $references;
        return $this;
    }

    public function addUsage($uses)
    {
        if (empty($uses)) {
            return null;
        }
        if (is_array($uses)) {
            foreach ($uses as $use) {
                $this->addUsage($use);
            }
            return $this;
        }
        $this->usages[] = $uses;

        return $this;

    }

    public function addTrait($trait)
    {
        if (empty($trait)) {
            return null;
        }
        if (is_array($trait)) {
            foreach ($trait as $trt) {
                $this->addTrait($trt);
            }
            return;
        }
        $this->traits[] = $trait;
        return $this;

    }

    public function addParameter($scope, $key, $value)
    {
        if (is_array($value)) {
            $value = "[\n" . collect($value)->map(function ($k, $v) {
                    return "        '" . $k . "'";
                })->implode(",\n") . "\n" . '    ]';
        } elseif (is_bool($value)) {
            $value = $value ? 'true' : 'false';
        } elseif (is_null($value)) {
            $value = 'null';
        } elseif (is_numeric($value)) {
            $value = "" . $value . "";
        } else {
            $value = "'" . $value . "'";
        }

        $this->params[$key] = ['scope' => $scope, 'value' => $value];
        return $this;

    }

    public function addConst($key, $value)
    {
        if (is_array($value)) {
            $value = "[\n" . collect($value)->map(function ($k, $v) {
                    return "        '" . $k . "'";
                })->implode(",\n") . "\n" . '    ]';
        } elseif (is_bool($value)) {
            $value = $value ? 'true' : 'false';
        } elseif (is_null($value)) {
            $value = 'null';
        } elseif (is_numeric($value)) {
            $value = "" . $value . "";
        } else {
            $value = "'" . $value . "'";
        }

        $this->consts[$key] = $value;
        return $this;
    }

    public function addMethod($scope, $name, $params = null, Array $code = null, $type = 'regular')
    {
        $params = is_array($params) ? implode(', ', $params) : $params;

        if ($code) {
            $code = '        ' . implode("        \n", $code);
        }

        $this->functions[] = ['name' => $name, 'params' => $params, 'code' => $code, 'scope' => $scope, 'type' => $type];

        return $this;
    }

    public function getNamespace()
    {
        return $this->namespace ? 'namespace ' . $this->namespace . ";\n" : null;
    }

    public function getExtension()
    {
        return $this->extension ? ' extends ' . $this->extension : null;
    }

    public function getImplementation()
    {
        return $this->implementation ? ' implements ' . $this->implementation : null;
    }

    public function getParamPropertyInfo()
    {
        /**
         * @property AccessRepository access
         */
        $sRet = "";
        foreach ($this->propertyInfo as $param => $refs) {
            $sRet .= " * @property $refs $param";
        }

        return !empty($sRet) ? implode("\n", [
            '/**',
            $sRet,
            ' */'
        ]) : null;
    }

    public function getMethods()
    {
        $methods = [];

        foreach ($this->functions as $data) {
            $method_name = $data['name'];

            switch ($data['type']) {
                default:
                case 'regular':
                    break;
                case 'mutator':
                    $method_name = 'set' . ucfirst(Str::camel($method_name)) . 'Attribute';
                    break;
                case 'accessor':
                    $method_name = 'get' . ucfirst(Str::camel($method_name)) . 'Attribute';
                    break;
            }

            $method = sprintf('    %s function %s(%s)', $data['scope'], $method_name, $data['params']);

            // if we are an interface we will ignore the body of each function
            if ($this->type == 'interface') {
                $code = [sprintf('%s;', $method)];
            } else {
                $code = [
                    $method,
                    '    {',
                    $data['code'],
                    "    }\n"
                ];
            }
            $methods[] = implode("\n", $code);


        }

        return count($this->functions) ? implode("\n", $methods) : '';

    }

    public function getParameters()
    {
        $sRet = null;
        foreach ($this->params as $key => $data) {
            $sRet .= '    ' . sprintf('%s $%s = %s;', $data['scope'], $key, $data['value']) . "\n";
        }

        return $sRet ? $sRet : null;
    }

    public function getConsts()
    {
        $sRet = null;
        foreach ($this->consts as $key => $data) {
            $sRet .= '    ' . sprintf('CONST %s = %s;', $key, $data) . "\n";
        }

        return $sRet ? $sRet : null;
    }

    public function getUsages()
    {
        $sRet = null;
        foreach ($this->usages as $usage) {
            $sRet .= 'use ' . $usage . ';' . "\n";
        }

        return $sRet;
    }

    public function getTraits()
    {
        $sRet = null;

        foreach ($this->traits as $trait) {
            $sRet .= '    use ' . $trait . ";\n";
        }

        return $sRet;

    }

    public function render()
    {
        $code = [
            '<?php',
            '// Created at ' . Carbon::now(),
            $this->getNamespace(),
            $this->getUsages(),
            $this->getParamPropertyInfo(),
            $this->type . ' ' . $this->name . $this->getExtension() . $this->getImplementation(),
            '{',
            $this->getTraits(),
            $this->getConsts(),
            $this->getParameters(),
            $this->getMethods(),
            '}'
        ];
        $sBuffer = "";
        foreach ($code as $block) {
            if (!empty($block)) {
                $sBuffer .= $block . "\n";
            }
        }

        $this->buffer = $sBuffer;
        return $this;
    }

    public function raw(array $code)
    {
        $this->buffer = implode("\n", $code);
    }

    public function save($override = false)
    {
        if (!file_exists($this->path) || $override) {
            if (!file_exists(dirname($this->path))) {
                mkdir(dirname($this->path), 0777, true);
            }
            $handle = fopen($this->path, 'w+');
            fwrite($handle, $this->buffer);
            fclose($handle);
        }
    }

    public function fileExists()
    {
        return file_exists($this->path);
    }
}
