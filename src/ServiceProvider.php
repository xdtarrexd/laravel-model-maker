<?php

namespace Tarre\ModelMaker;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{

    /**
     * Bootstrap the application services.
     */
    public function boot()
    {

        $this->publishes([
            __DIR__ . DIRECTORY_SEPARATOR . 'Config' . DIRECTORY_SEPARATOR . 'laravel-model-maker.php' => config_path('laravel-model-maker.php')
        ], 'laravel-model-maker');

    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Auto register consoles
        $this->app->singleton('command.make.orm', Console\MakeModelsConsole::class);
        $this->commands('command.make.orm');
    }
}
