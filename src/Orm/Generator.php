<?php
/**
 * Created by PhpStorm.
 * User: tarre
 * Date: 2017-04-07
 * Time: 15:31
 */

namespace Tarre\ModelMaker\Orm;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Tarre\ModelMaker\ClassMaker;
use Tarre\ModelMaker\Console\MakeModelsConsole as Console;
use Tarre\ModelMaker\Helper;

class Generator
{

    protected $console;
    protected $config;
    protected $re_eloquent_dates;


    public function __construct(Console $console)
    {

        if ($console->argument('debug')) {
            $console->comment('Loading configuration');
        }
        if (!Config::has('laravel-model-maker')) {
            $console->error('No config detected.');
            if ($console->confirm('Do you want to run php artisan vendor:publish --tag=laravel-model-maker --force ?')) {
                $console->call('vendor:publish', [
                    '--tag' => 'laravel-model-maker',
                    '--force' => true
                ]);
            } else {
                return null;
            }
        } else {
            if ($console->argument('debug')) {
                $console->comment('Configuration loaded successfully');
            }
        }

        $this->console = $console;
        $this->config = $this->parseConfig(Config::get('laravel-model-maker'));

        $this->createModels();

    }

    private function parseConfig($config)
    {
        // set defaults for timestamps
        $default_column_names = [
            'created_at' => 'created_at',
            'updated_at' => 'updated_at',
            'deleted_at' => 'deleted_at'
        ];
        foreach ($config['timestamps']['default_columns'] as $default_column_name => $value) {
            if (!$value) {
                $config['timestamps']['default_columns'][$default_column_name] = $default_column_names[$default_column_name];
            }
        }

        $this->re_eloquent_dates = implode('|', $config['timestamps']['default_columns']);

        return $config;
    }


    private function getDatabaseConnectionStructure()
    {

        $listTableColumns = function (\Illuminate\Database\Connection $DB, $table_name) {

            switch ($DB->getDriverName()) {
                case 'sqlsrv':
                    $table_column_query = $DB->select(DB::RAW("SELECT * FROM sys.columns WHERE object_id = OBJECT_ID('dbo." . $table_name . "')"));

                    $table_column_names = collect($table_column_query)->map(function ($k, $v) {
                        return $k->name;
                    });

                    break;
                default:

                    //$table_column_query = $DB->select(DB::RAW("SHOW COLUMNS FROM $table_name"));
                    $table_column_query = $DB->select(DB::RAW("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_SCHEMA = '" . $DB->getDatabaseName() . "' AND TABLE_NAME = '$table_name'"));

                    $table_column_names = collect($table_column_query)->map(function ($k, $v) {
                        return $k->COLUMN_NAME;
                    });
            }

            $table_columns = [];
            $table_columns_clean = [];
            foreach ($table_column_names as $k => $v) {
                $table_columns[$v] = $DB->getDoctrineColumn($table_name, $v);
                $table_columns_clean[$v] = $v;
            }


            return [
                'table_columns' => $table_columns,
                'table_columns_clean' => $table_columns_clean
            ];

        };


        //gets the initial rows for each table
        $rows = [];
        foreach ($this->config['connections'] as $connection_name => $data) {
            $connection_folder_name = ucfirst(preg_replace('/[^a-z]/i', DIRECTORY_SEPARATOR, $connection_name));

            if ($this->config['pop_first_directory']) {
                $connection_folder_name = explode(DIRECTORY_SEPARATOR, $connection_folder_name);
                array_shift($connection_folder_name);
                $connection_folder_name = implode(DIRECTORY_SEPARATOR, $connection_folder_name);
            }


            $DB = DB::connection($connection_name);
            if (!$DB->isDoctrineAvailable()) {
                $this->console->error('please require doctrine/dbal');
                break;
            }
            try {
                $platform = $DB->getDoctrineSchemaManager()->getDatabasePlatform();
            } catch (\Exception $e) {
                $this->console->error('Failed to parse connection "' . $connection_name . '". ' . $e->getMessage());
                continue;
            }

            $platform->registerDoctrineTypeMapping('enum', 'string');
            $platform->registerDoctrineTypeMapping('timestamp', 'datetime');

            $table_names = $DB->getDoctrineSchemaManager()->listTableNames();


            foreach ($table_names as $table_name) {
                $model_name = Helper::parse_macro($table_name, $this->config['name']);

                $listedTableColumns = $listTableColumns($DB, $table_name);
                $table_columns = $listedTableColumns['table_columns'];
                $table_columns_clean = $listedTableColumns['table_columns_clean'];


                // Determine if we have any timestamps
                $other_timestamps = [];
                $eloquent_timestamps = [];

                $primaryKey = null;

                foreach ($table_columns as $fillable => $data) {

                    $is_eloquent_timestamp = preg_match(sprintf('/%s/i', $this->re_eloquent_dates), $fillable);


                    if (is_a($data->getType(), 'Doctrine\DBAL\Types\DateTimeType') && !$is_eloquent_timestamp) {
                        $other_timestamps[] = $fillable;
                    } elseif ($is_eloquent_timestamp) {
                        $eloquent_timestamps[$fillable] = $fillable;
                    }

                    // get primarykey
                    if (!$primaryKey && $data->getAutoincrement()) {
                        $primaryKey = $fillable;
                    }
                }

                if (!$primaryKey) {
                    $primaryKey = $this->config['primary_key'];
                }

                $fillables = collect($table_columns)->except(collect($this->config['excluded_fillables'])->merge([$primaryKey])->toArray())->map(function ($k, $v) {
                    return $v;
                })->toArray();


                $desired_dir = $this->config['dir'] . DIRECTORY_SEPARATOR . $connection_folder_name;
                $namespace = $this->config['namespace'] . (empty($connection_folder_name) ? $connection_folder_name : DIRECTORY_SEPARATOR . $connection_folder_name);

                $desired_model_dir = $desired_dir . DIRECTORY_SEPARATOR . $model_name . '.php';

                $data = collect([
                    'namespace' => $namespace,
                    'desired_model_dir' => $desired_model_dir,
                    'fqdn' => $connection_folder_name . '\\' . $table_name,
                    'connection_folder_name' => $connection_folder_name,
                    'model_name' => $model_name,
                    'primarykey' => $primaryKey,
                    'table' => $table_name,
                    'connection' => $connection_name,
                    'table_columns' => $table_columns,
                    'table_columns_clean' => $table_columns_clean,
                    'fillable' => $fillables, // PDO hell
                    'eloquent_timestamps' => $eloquent_timestamps,
                    'other_timestamps' => $other_timestamps,
                    'relationships.belongstomany' => collect([]),
                    'relationships.belongstomanythrough' => collect([]),
                    'relationships.hasmany' => collect([]),
                    'relationships.belongsto' => collect([]),
                    'relationships.hasone' => collect([]),
                ]);
                $rows[] = $data;
            }

            $this->console->comment('Successfully parsed connection "' . $connection_name . '"');
        }

        $rows = collect($rows);


        return $rows;
    }


    private function createModels()
    {
        $rows = $this->getDatabaseConnectionStructure();

        if ($this->config['relationships']['enabled']) {
            $this->console->comment('Searching for relations with ' . count($rows) . ' tables, (nt*nt*nc), this may take a while if you have many tables');

            $RD = new RelationshipDetective($this->console, $rows, $this->config);
            $RDResult = $RD->getResult();
            $this->console->comment($RDResult['num_created_relations'] . ' relations found and created and ' . $RDResult['num_ignored_relations'] . ' ignored.');
        }

        $this->console->comment('Attempting to create ' . count($rows) . ' Models.');

        $created = 0;
        $already_exists = [];
        foreach ($rows as $row) {

            $class = new ClassMaker($row->get('desired_model_dir'));

            //speedup
            if ($class->fileExists()) {
                $already_exists[] = $row->get('desired_model_dir');
                continue;
            }
            $created++;

            $class->setNamespace($row->get('namespace'));
            $class->setExtension($this->config['extends']);

            $class->addUsage($this->config['uses']);

            if (count($row->get('other_timestamps'))) {
                $class->addUsage($this->config['timestamps']['provider']['uses']);
            }

            if (Helper::has($row->get('eloquent_timestamps'), ['deleted_at']) && $this->config['timestamps']['detect_softDeletes']) {
                $class->addUsage('Illuminate\Database\Eloquent\SoftDeletes');
                $class->addTrait('SoftDeletes');
            }

            $class->addTrait($this->config['traits']);

            $class->addParameter('protected', 'primaryKey', $row->get('primarykey'));
            $class->addParameter('protected', 'connection', $row->get('connection'));
            $class->addParameter('protected', 'table', $row->get('table'));

            $hasTimestamps = false;
            foreach ($this->config['timestamps']['default_columns'] as $const_name => $column_name) {
                $found_column_name = array_search($column_name, $row->get('eloquent_timestamps'));

                if ($found_column_name) {
                    $class->addConst(strtoupper($const_name), $found_column_name);

                    if ($const_name == 'updated_at' || $const_name == 'created_at') {
                        $hasTimestamps = true;
                    }
                } else {
                    $class->addConst(strtoupper($const_name), null);
                }

            }

            $class->addParameter('public', 'timestamps', $hasTimestamps);

            $class->addParameter('protected', 'fillable', $row->get('fillable'));
            $class->addParameter('protected', 'guarded', []);

            // Other timestamps
            foreach ($row->get('other_timestamps') as $name) {
                $class->addMethod('public', $name, ['$v'], [
                    'return ' . $this->config['timestamps']['provider']['usage'] . ';'
                ], 'accessor');
                // $class->addMethod('public', $name, ['$v'], [
                //    'return ' . $this->config['timestamps']['provider']['usage'] . ';'
                //], 'mutator');

            }


            $parent_model_first_word = preg_replace('/([A-Z]+[a-z]*).*/', '$1', $row->get('model_name'));

            // cache for preventing dupe-relations
            $cache = [];
            foreach ($this->config['relationships']['include'] as $relationship_name => $shouldInclude) {
                if ($shouldInclude) {
                    foreach ($row->get('relationships.' . $relationship_name) as $related_model) {

                        $m = $related_model['model'];
                        $foreignKey = $related_model['foreignKey'];
                        $otherOwnerKey = $related_model['otherOwnerKey'];
                        $pivot_table = $related_model['pivot_table'];

                        $name = "unknown";

                        switch ($relationship_name) {
                            case 'belongstomany':
                            case 'hasmany':
                                $name = Str::plural($m->get('model_name'));
                                break;
                            case 'belongsto':
                            case 'hasone':
                                $name = Str::singular($m->get('model_name'));
                                break;
                        }

                        $params = "";
                        switch ($relationship_name) {
                            case 'belongstomany':
                                $params = sprintf(", '%s'", $pivot_table);

                                break;
                            case 'hasmany':
                            case 'belongsto':
                            case 'hasone':
                                $params = sprintf(", %s, %s", $foreignKey ? "'$foreignKey'" : "null", $otherOwnerKey ? "'$otherOwnerKey'" : "null");
                                break;
                        }

                        //fix names if needed
                        if ($this->config['relationships']['exclude_parent_name'] && $parent_model_first_word != $name && Str::plural($parent_model_first_word) != $name) {
                            $name = preg_replace('/^' . $parent_model_first_word . '(.*)/', '$1', $name);
                        }

                        // add method if not exists

                        if (!isset($cache[$name])) {

                            $cache[$name] = true;
                            $class->addMethod('public', $name, null, [
                                "return \$this->" . Str::camel($relationship_name) . "('" . $m->get('namespace') . '\\' . $m->get('model_name') . "'" . $params . ");"
                            ]);
                        }

                    }
                }

            }

            // Save file
            $class->render()->save();
        }
        $this->console->comment('Created ' . $created . ' Models');
        if (count($already_exists)) {
            foreach ($already_exists as $already_exist) {
                $this->console->comment($already_exist . ' already exists.');
            }

        }

        $this->console->comment("");
    }


}
