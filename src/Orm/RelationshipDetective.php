<?php
/**
 * Created by PhpStorm.
 * User: tarre
 * Date: 2017-04-17
 * Time: 11:33
 */

namespace Tarre\ModelMaker\Orm;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Tarre\ModelMaker\Console\MakeModelsConsole as Console;
use Tarre\ModelMaker\Helper;


/**
 * @property Collection rows
 */
class RelationshipDetective
{

    protected $config;
    protected $console;
    protected $rows;
    protected $progressbar;
    protected $num_created_relations = 0;
    protected $num_ignored_relations = 0;


    public function __construct(Console $console, Collection &$rows, $config)
    {
        $this->console = $console;
        $this->config = $config;
        //assign rows locally
        $this->rows = $rows;

        //Start counter

        // following laravel standards, to find relations (belongstomany, belongsto and hasmany)
        $this->findStandardRelationships();

        // find user defined relationships
        $this->findCustomRelationships();


        // Finish bar

        //re-assign
        $rows = $this->rows;

        return $this;
    }

    protected function findStandardRelationships()
    {

        // Find belongsToMany relationships
        $pivotRelations = $this->getPivotRelations();

        foreach ($this->rows as $key => $row) {
            $found_pivot = array_search($row->get('table'), $pivotRelations['pivots']);
            if ($found_pivot) {
                $b2m = $pivotRelations['models'][$found_pivot];
                $b2p = $pivotRelations['pivots'][$found_pivot];

                $this->addRelation($b2m['model_a'], 'belongstomany', $b2m['model_b'], null, null, $b2p);
                $this->addRelation($b2m['model_b'], 'belongstomany', $b2m['model_a'], null, null, $b2p);
                $this->ignore($key); // we do not want this table to become a model
            }

        }

        //Find hasMany and belongsTo relationships
        foreach ($this->rows as $key => $row) {
            // Common names for relation keys
            $assumed_col_names = [
                $row->get('table') . 'id',
                Str::singular($row->get('table')) . '_id', // laravel standard
                Str::singular($row->get('table')) . 'id',
                Str::plural($row->get('table')) . '_id',
                Str::plural($row->get('table')) . 'id',
            ];

            $table_columns = $row->get('table_columns_clean');

            foreach ($this->rows as $key2 => $row2) {

                $table_columns2 = $row2->get('table_columns_clean');

                foreach ($assumed_col_names as $assumed_col_name) {
                    if ((preg_grep("/$assumed_col_name/i", $table_columns2) && $row->get('fqdn') !== $row2->get('fqdn'))) {

                        //find the real column key we want to reference
                        $real_table_column = Helper::array_search_keyInsensitive($assumed_col_name, $table_columns2);
                        $real_table_column2 = Helper::array_search_keyInsensitive($assumed_col_name, $table_columns);


                        $this->addRelation($row, 'hasmany', $row2, $real_table_column, $real_table_column2);
                        $this->addRelation($row2, 'belongsto', $row, $real_table_column, $real_table_column2);
                    }
                }

            }
        }


    }

    protected function findCustomRelationships()
    {
        foreach ($this->config['relationships']['map'] as $t1 => $t2) {
            $table_a = explode(':', $t1);// 0 = column, 1 = relationship
            $table_b = explode(':', $t2);// 0 = column, 1 = relationship
            $table_a_column = $table_a[0];
            $table_a_relationship = $table_a[1];

            $table_b_column = $table_b[0];
            $table_b_relationship = $table_b[1];


            foreach ($this->rows as $key => $row) {
                // get all table columns including primarykey
                $table_columns = $row->get('table_columns_clean');
                foreach ($this->rows as $key2 => $row2) {
                    $founded = false;
                    $table_columns2 = $row2->get('table_columns_clean');

                    //prevent from matching from self
                    if ($row->get('fqdn') !== $row2->get('fqdn')) {

                        switch ($this->config['relationships']['map_match_type']) {
                            case 'LOWERCASE':
                                if (
                                    preg_grep(sprintf("/%s/i", $table_a_column), $table_columns)
                                    &&
                                    preg_grep(sprintf("/%s/i", $table_b_column), $table_columns2)
                                ) {
                                    $founded = true;
                                }
                                break;

                            case 'STRICT':
                                if (
                                    preg_grep(sprintf("/%s/", $table_a_column), $table_columns)
                                    &&
                                    preg_grep(sprintf("/%s/", $table_b_column), $table_columns2)
                                ) {
                                    $founded = true;
                                }
                                break;
                            case 'SIMILAR_TEXT':
                                foreach ($table_columns as $table_column1) {
                                    foreach ($table_columns2 as $table_column2) {
                                        similar_text(strtolower($table_column1), strtolower($table_a_column), $p1);
                                        similar_text(strtolower($table_column2), strtolower($table_b_column), $p2);
                                        if ($p1 > 90 && $p2 > 90) {
                                            $founded = true;
                                        }
                                    }
                                }

                                break;
                            default:
                                $this->console->warn('Unkown matching type for relationshipfinder :( ');
                                break;
                        }
                    }

                    if ($founded) {
                        $col_a = Helper::array_search_keyInsensitive($table_a_column, $table_columns);
                        $col_b = Helper::array_search_keyInsensitive($table_b_column, $table_columns2);

                        //if not found, find the closest

                        $this->addRelation($row, $table_a_relationship, $row2, $col_a);
                        $this->addRelation($row2, $table_b_relationship, $row, $col_b);
                    }
                }

            }
        }
    }


    protected function addRelation(Collection $model_a, $type, Collection $model_b, $foreignKey = null, $otherOwnerKey = null, $pivot_table = null)
    {
        if ($this->console->argument('debug')) {
            $this->console->comment(sprintf('%s %s %s', $model_a->get('model_name'), $type, $model_b->get('model_name')));
        }

        $this->rows->where('fqdn', '=', $model_a->get('fqdn'))->first()->get('relationships.' . $type)->push([
            'model' => $model_b,
            'foreignKey' => $foreignKey,
            'otherOwnerKey' => $otherOwnerKey,
            'pivot_table' => $pivot_table
        ]);

        $this->num_created_relations++;
    }

    protected function ignore($key)
    {
        if ($this->console->argument('debug')) {
            $this->console->comment("Ignoring table: " . $this->rows[$key]->get('table'));
        }

        $this->rows->forget($key);
        $this->num_ignored_relations++;

    }

    protected function getPivotRelations()
    {
        $pivots = [];
        $models = [];

        foreach ($this->rows as $model_a) {
            foreach ($this->rows as $model_b) {
                $pivots[] = Helper::createPivotTable($model_a->get('table'), $model_b->get('table'));
                $models[] = ['model_a' => $model_a, 'model_b' => $model_b];
            }
        }

        return [
            'models' => $models,
            'pivots' => $pivots
        ];
    }

    public function getResult()
    {
        return [
            'num_created_relations' => $this->num_created_relations,
            'num_ignored_relations' => $this->num_ignored_relations,
        ];
    }
}
