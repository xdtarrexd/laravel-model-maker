## Was is das
Repository pattern made easy.

## Installation
1. `composer require tarre/laravel-model-maker --dev`
2. `php artisan vendor:publish --tag=laravel-model-maker --force`


## FAQ

**Q:** Nothing happends when i try to run the command

**A:** Then your version of laravel probably does not support package discovery, add `Tarre\ModelMaker\ServiceProvider::class` to your `config\app.php`
